#   Front-End de administracion de pedidos de cafeteria realizado como proyecto en el bootcamp FPUNA

## Clonar el repositorio
- git clone https://gitlab.com/bootcamp328/front-end-cafeteria.git
- cd front-end-cafeteria


## Instalar requerimientos
- npm install node

## Iniciar el servidor
- npm run dev
- Abrir el navegador en la direccion `http://localhost:5173/`
